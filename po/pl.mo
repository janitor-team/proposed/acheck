��            )         �     �  #   �     �     �               .  7   C     {     �     �     �  	   �  �   �       2   �  0   �  5   �     %     7  �  Q  	   �     �     	     !	     9	  Q   K	  C   �	  4   �	     
  c  
     ~  $   �     �  "   �     �     �        G   '      o  #   �     �  !   �     �  �        �  8   �  B     C   [     �  "   �  &  �     �          *  7   C     {  h   �  \   �  L   S     �                                                                                                        
                            	    %s version %s
 %s/%s:%s: `%s', unknown field name
 %s: %s %s: name %s
reserved word
 %s: spelling for `%s' %s: syntax error
 %s: value not valid
 Aspell Perl module not found, spelling check cancelled. Aspell: unable to use `%s'.
 Cannot backup `%s': %s
 Cannot read `%s': %s
 Cannot write to `%s': %s
 ERROR     Edit current line
Add hint
Skip the rest of the line
Exit and discard all changes
Add in dictionnary
Add lowercase in dictionnary
Ignore
Ignore all
 Empty file
 Enter correction to replace the highlighted part:
 Enter explanation, use displayed hint if empty:
 Exit and discard all changes! type `yes' to confirm:  Modify the line:
 Press 'Enter' to continue Usage: %s [OPTIONS] [INPUT_FILE]

options:
  -q, --quiet          quiet mode
  -v                   verbose, add more for more verbosity
      --verbose        set verbosity to n
      --rules <set>    use rules set <set>
      --type <type>    set filetype to <type>
  -i, --input <file>   input filename
  -o, --output <file>  output filename
  -s, --spell          check spelling with aspell
  -d, --dict <lang>    use <lang> dictionary
  -n, --nospell        don't check spelling
  -r, --review         add comments (for reviewers)
  -t, --trans          don't add comments (for translators)
      --dump           dump the rules and exit (for debugging)
  -V, --version        print version and exit
  -h, --help           print this message and exit

If input filename is '-' or not provided, data are read from STDIN
If output filename is not provided, input filename is used, '-' write to STDOUT

%s version %s
 WARNING   `%s': unknow offset
 `%s': unknown operation
 comment: no stop regex
 spelling for `%s' unable to resolve %s
name defined more than once as `rule', `list' and `comment'
 unable to resolve %s
undefined name as `rule', `list' or `comment'
 unable to resolve %s
undefined name as `valid' rule
 yes Project-Id-Version: acheck
Report-Msgid-Bugs-To: Nicolas Bertolissio <nico.bertol@free.fr>
POT-Creation-Date: 2003-08-18 20:24+0200
PO-Revision-Date: 2003-09-29 20:40+0100
Last-Translator: Arkadiusz Lipiec <alipiec@elka.pw.edu.pl>
Language-Team: Polish <pl@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 %s wersja %s
 %s/%s:%s: "%s", nieznana nazwa pola
 %s: %s %s: nazwa %s
zarezerwowane słowo
 %s: pisowania dla "%s" %: błąd składni
 %s: wartość nie jest poprawna
 Nie znaleziono modułu perla do aspella, anulowano sprawdzanie pisowni. Aspell: nie można użyć "%s".
 Nie można zarchiwizować "%s": %s
 Nie można odczytać "%s": %s
 Nie moÂżna zapisaĂŚ "%s": %s
 BŁĄD      Edycja bieżącego wiersza
Dodanie podpowiedzi
Pominięcie reszty wiersza
Zakończenie i porzucenie wszystkich zmian
Dodanie w słowniku
Dodanie małej litery w słowniku
Zignorowanie
Zignorowanie wszystkiego
 Puste pole
 Podaj poprawkę aby zastąpić podświetloną część:
 Podaj wyjaśnienie, użyj wyświetlonej podpowiedzi jeśli puste:
 Zakończ i porzuć wszystkie zmiany! wpisz "tak" aby potwierdzić:  Zmodyfikuj wiersz:
 Naciśnij "Enter" aby kontynuować Użycie: %s [OPCJE] [PLIK_WEJŚCIOWY]

opcje:
  -q, --quiet         tryb cichy
  -v                  tryb informacyjny, więcej informacji
      --verbose       ustawia informacyjność na n
      --rules <zbiór> używa zbioru <zbiór>
      --type <typ>    ustawia typ pliku na <typ>
  -i, --input <plik>  nazwa pliku wejściowego
  -o, --output <plik> nazwa pliku wyjściowego
  -s, --spell         sprawdza pisownię z aspellem
  -d, --disc <język>  ustawia <język> dla słownika
  -n, --nospell       nie sprawdza pisowni
  -r, --review        dodaje komentarze (dla korektorów)
  -t, --trans         nie dodaje komentarzy (dla tłumaczy)
      --dump          zrzuca zasady i kończy (do debugowania)
  -V, --version       wypisuje wersje i kończy
  -h, --help          wypisuje ten komunikat i kończy

Jeśli podany plik wejściowy to "-", wczytywane są dane ze
standardowego wejścia STDIN. Jeśli nie podano nazwy pliku wyjściowego
wówczas używana jest nazwa pliku wejściowego, używane jest "-"
jako wyjście standardowe STDOUT

%s wersja %s
 OSTRZEŻENIE   "%s": nieznane przesunięcie
 "%s": nieznana operacja
 komentarz: brak zatrzymującego wyrażenia regularnego
 pisownia dla "%s" Nie można odnaleźć nazwy %s
nazwa zdefiniowana więcej niż raz jako "zasada", "lista" i "komentarz"
 nie można odnaleźć nazwy %s
niezdefiniowana nazwa jako "zasada", "lista" lub "komentarz"
 Nie można odnaleźć nazwy %s
niezdefiniowana nazwa jako zasada "poprawna"
 tak 