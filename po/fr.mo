��            )         �     �  #   �     �     �               .  7   C     {     �     �     �  	   �  �   �       2   �  0   �  5   �     %     7  �  Q  	   �     �     	     !	     9	  Q   K	  C   �	  4   �	     
  �  
     �  (   �     �     �          "     :  F   O  '   �  &   �     �  %        +  �   7     	  =     J   U  N   �     �  !     �  &     �     �       5        T  ]   j  P   �  >        X                                                                                                        
                            	    %s version %s
 %s/%s:%s: `%s', unknown field name
 %s: %s %s: name %s
reserved word
 %s: spelling for `%s' %s: syntax error
 %s: value not valid
 Aspell Perl module not found, spelling check cancelled. Aspell: unable to use `%s'.
 Cannot backup `%s': %s
 Cannot read `%s': %s
 Cannot write to `%s': %s
 ERROR     Edit current line
Add hint
Skip the rest of the line
Exit and discard all changes
Add in dictionnary
Add lowercase in dictionnary
Ignore
Ignore all
 Empty file
 Enter correction to replace the highlighted part:
 Enter explanation, use displayed hint if empty:
 Exit and discard all changes! type `yes' to confirm:  Modify the line:
 Press 'Enter' to continue Usage: %s [OPTIONS] [INPUT_FILE]

options:
  -q, --quiet          quiet mode
  -v                   verbose, add more for more verbosity
      --verbose        set verbosity to n
      --rules <set>    use rules set <set>
      --type <type>    set filetype to <type>
  -i, --input <file>   input filename
  -o, --output <file>  output filename
  -s, --spell          check spelling with aspell
  -d, --dict <lang>    use <lang> dictionary
  -n, --nospell        don't check spelling
  -r, --review         add comments (for reviewers)
  -t, --trans          don't add comments (for translators)
      --dump           dump the rules and exit (for debugging)
  -V, --version        print version and exit
  -h, --help           print this message and exit

If input filename is '-' or not provided, data are read from STDIN
If output filename is not provided, input filename is used, '-' write to STDOUT

%s version %s
 WARNING   `%s': unknow offset
 `%s': unknown operation
 comment: no stop regex
 spelling for `%s' unable to resolve %s
name defined more than once as `rule', `list' and `comment'
 unable to resolve %s
undefined name as `rule', `list' or `comment'
 unable to resolve %s
undefined name as `valid' rule
 yes Project-Id-Version: acheck 0.3
Report-Msgid-Bugs-To: Nicolas Bertolissio <bertol@debian.org>
POT-Creation-Date: 2003-08-18 20:24+0200
PO-Revision-Date: 2003-08-18 15:20+0200
Last-Translator: Nicolas Bertolissio <bertol@debian.org>
Language-Team: French translators <debian-l10n-french@lists.debian.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=iso-8859-15
Content-Transfer-Encoding: 8bit
 %s version %s
 %s/%s:%s�: ��%s��, nom de champ inconnu
 %s�: %s %s�: nom ��%s��
mot r�serv�
 %s�: orthographe de ��%s�� %s�: erreur de syntaxe
 %s�: value invalide
 Module Perl pour Aspell non trouv�, correction orthographique annul�e. Aspell�: impossible d'utiliser ��%s��.
 Impossible de sauvegarder ��%s���: %s
 Impossible de lire ��%s���: %s
 Impossible d'�crire dans ��%s���: %s
 ERREUR      �diter la ligne actuelle
Ajouter le commentaire
Ignorer le reste de la ligne
Quitter en annulant toutes les modifications
Ajouter au dictionnaire
Ajouter en minuscules au dictionnaire
Ignorer
Ignorer toujours
 Fichier vide
 Saisissez la correction pour remplacer la partie surlign�e�:
 Saisissez une explication, le commentaire affich� sera utilis� � d�faut�:
 Quitter en annulant toutes les modifications�! Tapez ��oui�� pour confirmer�:  Modifiez la ligne�:
 Pressez ��Entr�e�� pour continuer Usage�: %s [OPTIONS] [FICHIER_D'ENTR�E]

options�:
  -q, --quiet             mode silencieux
  -v                      bavard, r�p�ter pour augmenter le bavardage
      --verbose           niveau de bavardage n
      --rules <jeu>       s�lection du jeu de r�gles <jeu>
      --type <type>       s�lection du type de fichier <type>
  -i, --input <fichier>   nom du fichier d'entr�e
  -o, --output <fichier>  nom du fichier de sortie
  -s, --spell             correction orthographique avec Aspell
  -d, --dict <langue>     utilisation du dictionnaire <langue>
  -n, --nospell           pas de correction orthographique
  -r, --review            ajout de commentaires (pour les relecteurs)
  -t, --trans             pas d'ajout de commentaires (pour les traducteurs)
      --dump              afficher les r�gles et quitter (pour le d�bogage)
  -V, --version           afficher la version et quitter
  -h, --help              afficher ce message et quitter

Si le nom du fichier d'entr�e est ��-�� ou n'est pas donn�, les donn�es sont lues depuis l'entr�e standard.
Si le nom du fichier de sortie n'est pas donn�e, celui d'entr�e est utilis�, ��-�� permet d'�crire sur la sortie standard.

%s version %s
 ATTENTION   ��%s���: d�calage inconnu
 ��%s���: op�ration inconnue
 comment�: pas d'expression rationnelle pour ��stop��
 orthographe de ��%s�� impossible de r�soudre ��%s��
nom de � rule �, � list � ou � comment � d�fini plusieurs fois
 impossible de r�soudre ��%s��
nom de � rule �, � list � ou � comment � ind�fini
 impossible de r�soudre ��%s��
nom de r�gle � valid � ind�fini
 oui 