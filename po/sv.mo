��            )         �     �  #   �     �     �               .  7   C     {     �     �     �  	   �  �   �       2   �  0   �  5   �     %     7  �  Q  	   �     �     	     !	     9	  Q   K	  C   �	  4   �	     
  i  
     �     �     �     �     �     �     �  <        Q  #   q     �     �     �  �   �     w  ,   �  ,   �  A   �          .  �  N     <     D     X     o     �  Y   �  N   �  :   G     �                                                                                                        
                            	    %s version %s
 %s/%s:%s: `%s', unknown field name
 %s: %s %s: name %s
reserved word
 %s: spelling for `%s' %s: syntax error
 %s: value not valid
 Aspell Perl module not found, spelling check cancelled. Aspell: unable to use `%s'.
 Cannot backup `%s': %s
 Cannot read `%s': %s
 Cannot write to `%s': %s
 ERROR     Edit current line
Add hint
Skip the rest of the line
Exit and discard all changes
Add in dictionnary
Add lowercase in dictionnary
Ignore
Ignore all
 Empty file
 Enter correction to replace the highlighted part:
 Enter explanation, use displayed hint if empty:
 Exit and discard all changes! type `yes' to confirm:  Modify the line:
 Press 'Enter' to continue Usage: %s [OPTIONS] [INPUT_FILE]

options:
  -q, --quiet          quiet mode
  -v                   verbose, add more for more verbosity
      --verbose        set verbosity to n
      --rules <set>    use rules set <set>
      --type <type>    set filetype to <type>
  -i, --input <file>   input filename
  -o, --output <file>  output filename
  -s, --spell          check spelling with aspell
  -d, --dict <lang>    use <lang> dictionary
  -n, --nospell        don't check spelling
  -r, --review         add comments (for reviewers)
  -t, --trans          don't add comments (for translators)
      --dump           dump the rules and exit (for debugging)
  -V, --version        print version and exit
  -h, --help           print this message and exit

If input filename is '-' or not provided, data are read from STDIN
If output filename is not provided, input filename is used, '-' write to STDOUT

%s version %s
 WARNING   `%s': unknow offset
 `%s': unknown operation
 comment: no stop regex
 spelling for `%s' unable to resolve %s
name defined more than once as `rule', `list' and `comment'
 unable to resolve %s
undefined name as `rule', `list' or `comment'
 unable to resolve %s
undefined name as `valid' rule
 yes Project-Id-Version: acheck 0.4
Report-Msgid-Bugs-To: Nicolas Bertolissio <nico.bertol@free.fr>
POT-Creation-Date: 2003-08-18 20:24+0200
PO-Revision-Date: 2005-10-06 08:57+0200
Last-Translator: Daniel Nylander <po@danielnylander.se>
Language-Team: Swedish <sv@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=iso-8859-1
Content-Transfer-Encoding: 8bit %s version %s
 %s/%s:%s: `%s', ok�nt f�ltnamn
 %s: %s %s: namn %s
reserverade ord
 %s: stavning f�r `%s' %s: syntaxfel
 %s: v�rde inte giltigt
 Aspell Perl-modul hittades inte, stavningskontroll avbruten. Aspell: kan inte anv�nda `%s'.
 Kan inte s�kerhetskopiera `%s': %s
 Kan inte l�sa `%s': %s
 Kan inte skriva till `%s': %s
 FEL �ndra nuvarande rad
L�gg till tips
Hoppa �ver resten av raderna
Avsluta och kasta alla �ndringar
L�gg till i ordbok
L�gg till gemener i ordbok
Ignorera
Ignorera alla
 Tom fil
 Ange r�ttning att byta ut mot framh�vd del:
 Ange f�rklarning, anv�nd visat tips om tom:
 Avsluta och kasta alla �ndringar! skriv 'ja' f�r att g�ra detta:  Modifiera raden:
 Tryck 'Enter' f�r att forts�tta Anv�ndning: %s [OPTIONS] [INPUT_FILE]

options:
  -q, --quiet          tyst l�ge
  -v                   informativt, l�gg till fler f�r mer information
      --verbose        s�tt information till n
      --rules <set>    anv�nd regelupps�ttning <set>
      --type <type>    s�tt filtyp till <type>
  -i, --input <file>   mata in filenamn
  -o, --output <file>  utg�ng filnamn
  -s, --spell          kontrollera stavning med aspell
  -d, --dict <spr�k>   anv�nd <spr�k> ordbok
  -n, --nospell        kontrollera inte stavning
  -r, --review         l�gg till kommentar (f�r granskare)
  -t, --trans          l�gg inte till kommentarer (f�r �vers�ttare)
      --dump           dumpa reglerna och avsluta (f�r avlusning)
  -V, --version        skriv ut version och avsluta
  -h, --help           skriv ut detta meddelande och avsluta

Om ing�ngsfilnamn �r '-' eller inte medskickat, data blir l�st fr�n STDIN
Om utg�ngsfilnamn inte skickas med anv�nds ing�ngsfilnamnet, '-' skriv till STDOUT

%s version %s
 VARNING `%s': ok�nd offset
 `%s': ok�nd operation
 kommentar: inget stoppregex
 stavning f�r '%s' kunde inte sl� upp %s
namn definierat mer �n en g�ng som 'regel' 'lista' och 'kommentar'
 kunde inte sl� upp %s
odefinierat namn som 'regel', 'lista' eller 'kommentar'
 kunde inte sl� upp %s
odefinierat namn som 'giltig' regel
 ja 