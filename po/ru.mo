��            )         �     �  #   �     �     �               .  7   C     {     �     �     �  	   �  �   �       2   �  0   �  5   �     %     7  �  Q  	   �     �     	     !	     9	  Q   K	  C   �	  4   �	     
  �  
       9   !     [  <   b  ;   �  .   �  .   
  g   9  =   �  L   �  2   ,  3   _     �  e  �     
  h   !  �   �  t     +   �  6   �  �  �     �  ,   �  0   �  h     7     �   �  |   J  o   �     7                                                                                                        
                            	    %s version %s
 %s/%s:%s: `%s', unknown field name
 %s: %s %s: name %s
reserved word
 %s: spelling for `%s' %s: syntax error
 %s: value not valid
 Aspell Perl module not found, spelling check cancelled. Aspell: unable to use `%s'.
 Cannot backup `%s': %s
 Cannot read `%s': %s
 Cannot write to `%s': %s
 ERROR     Edit current line
Add hint
Skip the rest of the line
Exit and discard all changes
Add in dictionnary
Add lowercase in dictionnary
Ignore
Ignore all
 Empty file
 Enter correction to replace the highlighted part:
 Enter explanation, use displayed hint if empty:
 Exit and discard all changes! type `yes' to confirm:  Modify the line:
 Press 'Enter' to continue Usage: %s [OPTIONS] [INPUT_FILE]

options:
  -q, --quiet          quiet mode
  -v                   verbose, add more for more verbosity
      --verbose        set verbosity to n
      --rules <set>    use rules set <set>
      --type <type>    set filetype to <type>
  -i, --input <file>   input filename
  -o, --output <file>  output filename
  -s, --spell          check spelling with aspell
  -d, --dict <lang>    use <lang> dictionary
  -n, --nospell        don't check spelling
  -r, --review         add comments (for reviewers)
  -t, --trans          don't add comments (for translators)
      --dump           dump the rules and exit (for debugging)
  -V, --version        print version and exit
  -h, --help           print this message and exit

If input filename is '-' or not provided, data are read from STDIN
If output filename is not provided, input filename is used, '-' write to STDOUT

%s version %s
 WARNING   `%s': unknow offset
 `%s': unknown operation
 comment: no stop regex
 spelling for `%s' unable to resolve %s
name defined more than once as `rule', `list' and `comment'
 unable to resolve %s
undefined name as `rule', `list' or `comment'
 unable to resolve %s
undefined name as `valid' rule
 yes Project-Id-Version: acheck 0.5.5
Report-Msgid-Bugs-To: Nicolas Bertolissio <nico.bertol@free.fr>
PO-Revision-Date: 2018-04-03 06:21+0300
Last-Translator: Galina Anikina <merilaga@yandex.ru>
Language-Team: Russian <debian-l10n-russian@lists.debian.org>
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.6
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 %s версия %s
 %s/%s:%s: «%s», неизвестное имя поля
 %s: %s %s: имя %s
зарезервированное слово
 %s: проверка правописания для «%s» %s: синтаксическая ошибка
 %s: неправильное значение
 Модуль Perl aspell не найден; проверка правописания отменена. Aspell: не удалось использовать «%s».
 Не удалось сделать резервную копию «%s»: %s
 Не удалось прочитать «%s»: %s
 Не удалось записать в «%s»: %s
 ОШИБКА     Редактировать текущую строку
Добавить подсказку
Пропустить остаток строки
Выйти и сбросить все изменения
Добавить в словарь
Добавить в словарь в нижнем регистре
Игнорировать
Игнорировать все
 Пустой файл
 Введите исправление для замены выделенной части текста:
 Введите объяснение; оставьте пустым для использования отображаемой подсказки:
 Выйти и сбросить все изменения! Введите «yes» для подтверждения:  Отредактируйте строку:
 Для продолжения нажмите «Enter» Использование: %s [ПАРАМЕТРЫ] [ВХОДНОЙ ФАЙЛ]

параметры:
  -q, --quiet          тихий режим работы
  -v                   подробный; используйте несколько раз 
                       для большей детализации
      --verbose        установить уровень подробности в n
      --rules <set>    использовать набор правил <set>
      --type <type>    установить тип файла в <type>
  -i, --input <file>   имя входного файла
  -o, --output <file>  имя выходного файла
  -s, --spell          проверить правописание с aspell
  -d, --dict <lang>    использовать словарь <lang>
  -n, --nospell        не проверять правописание
  -r, --review         добавить комментарии (для рецензентов)
  -t, --trans          не добавлять комментарии (для переводчиков)
      --dump           сбросить правила и выйти (для отладки)
  -V, --version        напечатать версию и выйти
  -h, --help           напечатать это сообщение и выйти

Если имя входного файла установлено в «-» или отсутствует, то данные читаются из STDIN
Если имя выходного файла отсутствует, то будет использовано имя входного файла; укажите «-» для вывода в STDOUT

%s версия %s
 ВНИМАНИЕ   «%s»: неизвестный отступ
 «%s»: неизвестная операция
 комментарий: отсутствует регулярное выражение останова
 проверка правописания для «%s» невозможно разрешить %s
имя определено более чем один раз как «rule», «list» и «comment»
 невозможно разрешить %s
неопределённое имя как «rule», «list» или «comment»
 невозможно разрешить %s
неопределённое имя как правило «valid»
 yes 