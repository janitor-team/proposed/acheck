#!/usr/bin/perl -w

=encoding ISO8859-1

=head1 NOM

acheck - Correction des erreurs courantes de traduction

=head1 SYNOPSIS

acheck [OPTIONS] [FICHIER_D'ENTR�E]

=head1 DESCRIPTION

Ce programme analyse un fichier en v�rifiant des r�gles de syntaxe et
�ventuellement en demandant � Aspell de v�rifier l'orthographe des mots. Il
sugg�re des corrections et g�n�re un fichier corrig� en fonction, ajoutant des
commentaires de relecture si vous le d�sirez.

Il essaye de trouver le type de fichier en fonction de son extension ou de ses
premi�res lignes et charge les r�gles correspondantes.

Il affiche les lignes au fur et � mesure de l'analyse.

Quand une erreur est trouv�e, un menu est affich�. Pressez simplement Entr�e si
vous ne voulez rien modifier. Si un choix vous convient, entrez la lettre
correspondante. Si vous voulez corriger mais qu'aucun choix n'est correct,
entrez un espace, il vous demandera alors un texte pour remplacer celui qui est
surlign�. Il remplacera le texte surlign� par votre choix et reprendra
l'analyse.

Voici toutes les commandes disponiblesE<nbsp>:

=over 4

=item I<Enter>, ignorer.

Ignore l'erreur.

=item I<Ctrl+L>, rafficher.

Raffiche la derni�re ligne, les suggestions et commentaires.

=item I<Espace>, �diter.

�dite le texte surlign�.

=item I<E>, �diter la ligne.

�dite la ligne enti�re.

=item I<H>, ajouter le commentaire.

Ajouter le commentaire affich� en tant que commentaire de relecture. Ceci peut
�tre utilis� si vous voulez que le traducteur voit l'avertissement ou l'erreur
mais que vous n'avez pas de correction � apporter.

=item I<N>, ligne suivante.

Continuer � la ligne suivante en sautant le reste de la ligne courante.

=item I<X>, quitter sans changement.

Quitter sans sauvegarder les modifications, le script vous demande alors
confirmation, vous devez tapez �E<nbsp>B<yes>E<nbsp>� (oui) pour sortir sinon
l'analyse reprend sur l'erreur actuelle.

=item I<a>, ajouter au dictionnaire.

Ajoute le mot surlign� � votre dictionnaire personnel, avec les majuscules
telles qu'elles sont.

=item I<l>, ajouter en minuscules au dictionnaire.

Ajoute le mot surlign� � votre dictionnaire personnel, en minuscules.

=item I<i>, ignorer le mot.

Ignore le mot surlign�, comme I<Entr�e>.

=item I<I>, ignorer toujours.

Ignore le mot surlign� et l'ajoute � votre dictionnaire de session.

=back

=head1 OPTIONS

Niveau de bavardageE<nbsp>:

=over 4

=item I<-q>, I<--quiet>

silencieuxE<nbsp>;

=item I<-v>

verbeux, commence au niveau I<$Debug + 1>, plus il y en a, plus il est bavard
(voir plus bas)E<nbsp>;

=item I<--verbose n>

utiliser le niveau I<n> (voir plus bas).

=back

FichiersE<nbsp>:

=over 4

=item I<-i>, I<--input>

nom du fichier d'entr�e, utiliser �E<nbsp>I<->E<nbsp>� pour lire depuis
l'entr�e standardE<nbsp>;

=item I<-o>, I<--output>

nom du fichier de sortie, utiliser �E<nbsp>I<->E<nbsp>� pour �crire sur la
sortie standard. Si aucun nom de fichier de sortie n'est fourni, le fichier
d'entr�e est sauvegard� avec l'extension �E<nbsp>I<bak_ext>E<nbsp>� et son nom
est utilis�.

=back

V�rification de l'orthographeE<nbsp>:

=over 4

=item I<-s>, I<--spell>

v�rifier l'orthographe avec AspellE<nbsp>;

=item I<-d langue>, I<--dict langue>

utiliser Aspell avec le dictionnaire I<langue>E<nbsp>;

=item I<-n>, I<--nospell>

ne pas v�rifier l'orthographe.

=back

ModeE<nbsp>:

=over 4

=item I<-r>, I<--review>

mode relecture, ajoute des commentaires sur des lignes commen�ant par
I<$Comment> apr�s les lignes analys�esE<nbsp>;

=item I<-t>, I<--trans>

mode traduction, ne pas ajouter de commentaire, ne faire que les corrections.

=back

AutreE<nbsp>:

=over 4

=item I<--rules jeu_de_r�gles>

utiliser I<jeu_de_r�gles> comme jeu de r�glesE<nbsp>;

=item I<--type type_de_fichier>

utiliser I<type_de_fichier> quelque soit le type de fichierE<nbsp>;

=item I<--dump>

affiche le jeu de r�gles, pour le d�bogageE<nbsp>;

=item I<-V>, I<--version>

afficher le num�ro de version.

=item I<-h>, I<--help>

afficher un court message d'utilisationE<nbsp>;

=back

=head2 Niveaux de bavardage

=over 4

=item I<0> silencieux, normal

seulement les avertissements et les erreursE<nbsp>;

=item I<1> d�bogage

noms des proc�duresE<nbsp>;

=item I<2> d�bogage bavard

noms et arguments des proc�duresE<nbsp>;

=item I<3> .. I<5> d�bogage tr�s bavard

d�tails des analyses et des v�rifications.

=back

=head1 VOIR AUSSI

acheck(5), acheck-rules(5)

=head1 AUTEUR

Nicolas Bertolissio <bertol@debian.org>
  
=cut
